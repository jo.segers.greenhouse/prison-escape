from logic import engine, context, location
from content import prison

player = context.Player(input('Please enter your name: '))

context = context.Context(player)

start_location = prison.Cell(context)

engine.Engine().start(context, start_location)
