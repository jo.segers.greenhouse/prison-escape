from logic import location

class Cell(location.Location):
    def __init__(self, context):
        description = lambda context : f'''Hi {context.player.name}
The first goal of this game is to escape your cell, find your belongings, arm yourself with a gun and get the hell out of this prison.       
You are standing in an empty cell. You see a bed and a small cabinet.'''
        movement_choices = [
            location.Movement_Choice('a', 'Go through the cell door into the hallway', self.has_cell_key, lambda context: Hallway(context))
        ]
        action_choices = [
            location.Action_Choice('1', 'Search cabinet', lambda: not self.has_cell_key(), self.search_cabinet)
        ]

        super().__init__(context, description, movement_choices, action_choices)

    def has_cell_key(self):
        return "cell_key" in self.context.items

    def search_cabinet(self):
        self.context.items.append('cell_key')
        return 'You spend some time searching the cabinet and find a key in a hidden compartment.'


class Hallway(location.Location):
    def __init__(self, context):
        description = lambda context : '''You are standing in a long hallway. The walls are lined with empty cells and there is a small iron hatch in the floor. 
You think you might be able to access the sewers through this hatch, but it is rusted shut. You need some tool to to pry it open.
Only the door to your own cell is open.
There is also a staircase leading up and a locked door to the courtyard.'''
        movement_choices = [
            location.Movement_Choice('a', 'Go back into your own cell', lambda: True, lambda context: Cell(context)),
            location.Movement_Choice('b', 'Take the staircase upwards', lambda: True, lambda context: Top_Of_Staircase(context)),
            location.Movement_Choice('c', 'Pry open the hatch and head into the sewers', self.has_crowbar, lambda context: Sewer(context))
        ]
        super().__init__(context, description, movement_choices, [])
    
    def has_crowbar(self):
        return "crowbar" in self.context.items


class Top_Of_Staircase(location.Location):
    def __init__(self, context):
        description = lambda context: '''You stand at the top of the staircase on a small platform overlooking the hallway below. 
Before you is a metal door, it appears to be unlocked.'''
        movement_choices = [
            location.Movement_Choice('a', 'Head downstairs into the hallway', lambda: True, lambda context: Hallway(context)),
            location.Movement_Choice('b', 'Open the door and head inside', lambda: True, lambda context: Guard_Room(context))
        ]
        super().__init__(context, description, movement_choices, [])

class Guard_Room(location.Location):
    def __init__(self, context):
        description = lambda context: '''You are in the guardroom. Nobody is here. You see a desk with a computer and two chairs.
There are 2 drawers in the desk.
The back wall is lined with lockers and a toolbox is standing in the corner of the room.'''

        movement_choices = [
            location.Movement_Choice('a', 'Go back outside through the door', lambda: True, lambda context: Top_Of_Staircase(context))
        ]

        action_choices = [
            location.Action_Choice('1', 'Search the desk', lambda: not self.has_gun(), self.search_desk),
            location.Action_Choice('2', 'Search the lockers', lambda: not self.has_belongings(), self.search_lockers),
            location.Action_Choice('3', 'Search the toolbox', lambda: not self.has_crowbar(), self.search_toolbox)
        ]

        super().__init__(context, description, movement_choices, action_choices)

    def search_desk(self):
        self.context.items.append('gun')
        return 'You search the desk and to your surprise you find a loaded gun in one of the drawers.'
    
    def search_lockers(self):
        self.context.items.append('belongings')
        self.context.items.append('uniform')
        return '''You open the lockers and find a lot of clothing, you keep looking and eventually you find your own clothes and a uniform from one of the guards.
Time to get out of these prison clothes.'''

    def search_toolbox(self):
        self.context.items.append('crowbar')
        return '''The first thing you see after opening the toolbox is a crowbar, without hesitation you add it to your inventory.
This could be usefull in the future.'''

    def has_gun(self):
        return "gun" in self.context.items
    
    def has_belongings(self):
        return "belongings" in self.context.items

    def has_crowbar(self):
        return "crowbar" in self.context.items


class Sewer(location.Location):
    def __init__(self, context):
        movement_choices = [
            location.Movement_Choice('a', 'Go back into the hallway', lambda: True, lambda context: Hallway(context)),
            location.Movement_Choice('x', 'You have won the game', self.is_victory, None)
        ]

        super().__init__(context, None, movement_choices, [])

    def get_description(self):
        if self.is_victory():
            return f'''Congratulations {self.context.player.name}
You have your stuff back, you are armed with a gun and disguised as a prison guard!
You escape throug the sewer and prepare to get your revenge. Stay tuned for the next exciting episode!'''
        else:
            return '''You need your belongings, a gun and a uniform in order to escape this prison otherwise the cops ouside will arrest you.
Find these items first, then use these sewers to escape.'''

    def is_victory(self):
        return "belongings" in self.context.items and "uniform" in self.context.items and "gun" in self.context.items