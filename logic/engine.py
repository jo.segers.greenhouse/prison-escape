class Engine:
    def start(self, context, start_location):
        self.context = context
        location = start_location

        while(location != None):
            self.print('----------\nYou have the following items in your possession:')

            for item in context.items:
                self.print(item)
            self.print('---')

            self.print(location.get_description())

            movement_choices = location.get_movement_choices()
            action_choices = location.get_action_choices()

            for movement_choice in movement_choices:
                self.print(f'{movement_choice.index} -> {movement_choice.description}')
            
            for action_choice in action_choices:
                self.print(f'{action_choice.index} = {action_choice.description}')

            chosen_index = input('What would you like to do? ')

            self.process_action(chosen_index, action_choices)
            location = self.process_location(chosen_index, movement_choices, location)

        self.print('Game over')
    
    def process_action(self, chosen_index, action_choices):
        for action_choice in action_choices:
            if (action_choice.index.lower() == chosen_index.lower()):
                self.print(action_choice.action())

    def process_location(self, chosen_index, movement_choices, current_location):
        new_location = current_location
        for movement_choice in movement_choices:
            if (movement_choice.index.lower() == chosen_index.lower()):
                if (movement_choice.link == None):
                    new_location = None
                else :
                    new_location = movement_choice.link(self.context)

        return new_location

    def print(self, text):
        print(text)
