class Context:
    def __init__(self, player):
        self.player = player
        self.items = []
        self.events = []

class Player:
    def __init__(self, name):
        self.name = name