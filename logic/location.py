class Location:
    def __init__(self, context, description, movement_choices, action_choices):
        self.context = context
        self.description = description
        self.movement_choices = movement_choices
        self.action_choices = action_choices

    def get_description(self):
        return self.description(self.context)

    def get_movement_choices(self):
        valid_choices = []
        for choice in self.movement_choices:
            if (choice.check()):
                valid_choices.append(choice)
        return valid_choices

    def get_action_choices(self):
        valid_choices = []
        for choice in self.action_choices:
            if (choice.check()):
                valid_choices.append(choice)
        return valid_choices

class Movement_Choice:
    def __init__(self, index, description, check, link):
        self.index = index
        self.description = description
        self.check = check
        self.link = link

class Action_Choice:
    def __init__(self, index, description, check, action):
        self.index = index
        self.description = description
        self.check = check
        self.action = action
